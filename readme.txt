++++++++
|README|
--------

Overview:
---------

This code-base contains a collection of Matlab (C) scripts (currently
compatible with Octave) to process database short Fourier transforms
(SFDB).

Structure
---------

- Graph.m is a static class that contains most of various plots used.
- Sfdb.m is a class to generate an SFDB object from files containing
peakmaps and SFTs objects similarly found in Peakmap.m and
SfdbBlock.m respectively.
- Signal.m is a static class for various signal processing algorithms
to support this codebase or for testing or understanding DSP
concepts. Most of its functionality can be found implemented in
other 1st or 3rd party libraries
- Private/ is a folder of functions currently outside of the scope of
this code-base but needed nonetheless.
- See relevant README sections for test_suite, tests/, and config.m

Example:
--------
The best way to show off this code base is by running demos with
test_suite (See Tests & Demos)

Load configuration
> [fileF, fileP] = config();
Read first SFDB block from data specified in config
> sfts = Sfdb.read(fileF, fileP, 1);
Regenerate a power spectrum
> spec = sfts.block{1}.generate_pow_spec();

Config:
-------
In config.m change path variables to appropriate data locations.  This
is needed to run any of the demos. The tests however all use their own
mock data.

Tests & Demos:
--------------
Running test_suite() will run basic tests as well as demos.
To run the default tests:

> test_suite()

To run all the demos:

> test_suite('demo')


To specify a demo or test put it as an argument into test suite. For
example if you want to run an injection demo use:

> test_suite('inject')

To demo a graph the current data:

> test_suite('graphs')

To run the mock data graphs:

> test_suite('testGraph')

If you want to run everything available use:

> test_suite('all')

Doppler Injections:
-------------------

Navigating to the 'tests' directory one can run 'doppler_close' and 'doppler_far'
which will inject doppler shifted signals either very close or very far from each other.

Dependencies:
-------------

Currently this code base can run on Matlab or Octave without any
required libraries.

However, there are optimizations, more tests, better demos, updated
code use when the following packages or Matlab toolboxes are
installed:

Base:
- Snag2
Performance:
- Signal
Tests:
- Signal
- Communications
