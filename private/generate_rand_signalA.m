function [ source ] = generate_rand_signalA(minf,maxf )
    
  rr=O2LL;

    minff=85;
    maxff=116;
    minsd=-1e-15;%-8;
    maxsd=0;%-8;
    mindd_f=-2.573995227306916e-18*1e1;%orignal number to the -18 power is 1000 times than 2*Tobs*sd
    maxdd_f=2.573995227306916e-18*1e1;
    minmjd=rr.ini;  %5.728359684020000e+04;
    maxmjd=rr.fin;   %5.737359684020000e+04;
    minlon=0;
    maxlon=360;
    minlat=-90;
    maxlat=90;
    mineta=-1;
    maxeta=1;
    minpsi=-90;
    maxpsi=90;
    minamp=1e-24;%1e-25;
    maxamp=5e-24;%1e-24,1e-23;


if ~exist('minf','var')  
    minf=minff;
    maxf=maxff;   
end

freq=pick_one_uni_dist(minf,maxf);
d_freq=pick_one_uni_dist(minsd,maxsd);
%%dd_freq=pick_one_uni_dist(mindd_f,maxdd_f);
mjdtime=pick_one_uni_dist(minmjd,maxmjd);
alpha=pick_one_uni_dist(minlon,maxlon);
delta=pick_one_uni_dist(minlat,maxlat); 
eta=pick_one_uni_dist(mineta,maxeta);
psi=pick_one_uni_dist(minpsi,maxpsi);
amps=pick_one_uni_dist(minamp,maxamp);

dd_freq=0; %%%NO second spin down.

[ecl_lon ecl_lat]=astro_coord('equ','ecl',alpha,delta);

location_ecl=[ecl_lon ecl_lat];

source.a=alpha;
source.d=delta;
source.v_a=0;
source.v_d=0;
source.pepoch=mjdtime; %not really relevant, only relevant in the case where the position of the source is changing with time (has some velocity)
source.f0=freq;
source.df0=d_freq;
source.ddf0=dd_freq;
source.fepoch=mjdtime;
source.ecl=location_ecl;
source.t00=51544;
source.eps=1;
source.eta=eta;
source.psi=psi;
source.h=amps;
source.snr=1;
source.coord=0;
source.chphase=0;
source.dfrsim=-0.200000000000000;


end

