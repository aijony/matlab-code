function [simN]=AddsignalsinoneFFT_subs(piahead2,dnu,N,PAR,simsour,nfigD,dsfact)
  %TO  BE TOTALLY VERIFIED !!!!
  nfig=1;
  if ~exist('nfigD','var')
    nfigD=1
  end
  if ~exist('dsfact','var')
    dsfact=2048; %256 for the first band,1024. to verify
  end
  dt=piahead2.tsamplu; %sampling time of the time data,
  dtori=dt;
  lfftori=piahead2.nsamples*2;
  lfftoriU=piahead2.nsamples*2;
  mjd=piahead2.mjdtime;
  SD=86164.09053083288;
  nsid=10000;
  dfr=piahead2.deltanu;
  if dsfact >1
    dtori=dtori*dsfact;
    lfftori=lfftori/dsfact;
  end
  %This depends only on the first FFT, so can be done only once but
  %parameters need to be stored
  %[PARS,t0,sour,f00,f0,df0,ddf0,simsour,antenna,simamp,eta,psi,fi,Hp,Hc]=SETPARMS(many_simpar,N,piahead2INI);

  %
  t0=PAR(1).t0;
  Dt0=(mjd-t0)*86400; %important. Time difference between FFTs
  for ss=1:N
    format long
    f0(ss)=PAR(ss).f0;
    df0(ss)=PAR(ss).df0;
    ddf0(ss)=PAR(ss).ddf0;
    Hp(ss)=PAR(ss).Hp;
    Hc(ss)=PAR(ss).Hc;
    antenna(ss)=PAR(ss).antenna;
    simamp(ss)=PAR(ss).simamp;

    % select 1 Hz around the ss injection freq
    k1=floor((floor(f0(ss)))/dfr+0.0001)+1;
    k2=k1+lfftori-1;
    %%k2=round((f0(ss)+0.5)/dfr+0.0001);%+1; %took aout to make size(sft(k1:k2)==8192
    %     if floor((k2-k1)/2)*2 == k2-k1
    %         k2=k2+1;
    %     end

    %     if dsfact >1
    %         if k2>lfftoriU
    %             k2=lfftoriU;
    %             if floor((k2-k1)/2)*2 == k2-k1
    %                 k2=k2-1;
    %             end
    %         end
    %     end

    %fprintf('k1,k2= %d,%d %d \n',k1,k2,k2-k1)
    fr1=(k1-1)*dfr;
    fr2=(k2-1)*dfr;
    NORM=1;
    if dsfact >1
%%%%%%frequency bins with respect to the integer start of the band%
      f0int=floor(fr1);
      k1int=(fr1-f0int)/dfr+1;
      k2int=(fr2-f0int)/dfr+1;
      f1sub=(k1int-1)*dfr;
      f2sub=(k2int-1)*dfr;
      %fprintf('SubBand: %10.10f %10.10f\n',f1sub,f2sub);
      NORM=sqrt(dsfact*dsfact)/2;
    end
    % end select 1 Hz around


    teph=mjd+dt/86400; % time at center+1 sample
    sour1(ss)=new_posfr(simsour(ss),teph);
    r=astro2rect([sour1(ss).a sour1(ss).d],0);
    v(1)=piahead2.vx_eq;
    v(2)=piahead2.vy_eq;
    v(3)=piahead2.vz_eq;
    p(1)=piahead2.px_eq;
    p(2)=piahead2.py_eq;
    p(3)=piahead2.pz_eq;
    pos1=p(1)+v(1)*dtori*(-lfftori/2:lfftori/2-1);
    pos2=p(2)+v(2)*dtori*(-lfftori/2:lfftori/2-1);
    pos3=p(3)+v(3)*dtori*(-lfftori/2:lfftori/2-1);
    pos=pos1*r(1)+pos2*r(2)+pos3*r(3);
    [ A0 A45 Al Ar sid1 sid2]=check_ps_lf(simsour(ss),antenna(ss),nsid,0);
    tt=Dt0+dtori*(0:lfftori-1)+tdt2tdb(mjd); %time conversion to SS barycenter, 0:num_of_samples_per_FFT*2, dtori=sampling time per FFT

    ph1=mod((f0(ss)*tt+df0(ss)*(tt.^2)/2+ddf0(ss)*(tt.^3)/6),1)*2*pi; % simulates frequency with spin-down, impact on phase to 2nd order
    f0a=(f0(ss)+df0(ss)*tt);
    ph2=f0a.*pos*2*pi;  % Romer
    ph=mod(ph2+ph1,2*pi); %add phase due to second order s.d. and due to Roemer effect
    st=gmst(mjd)+dtori*(86400/SD)*(0:lfftori-1)/3600; %daily effect of earth's rotation
    i1=mod(round(st*(nsid-1)/24),nsid-1)+1;
    e0=exp(1j*ph);
    % simREAL=simamp(ss)*real((Hp(ss)*sid1(i1)+Hc(ss)*sid2(i1)).*e0); %sidereal modulation of hplus and hcross
    sim=simamp(ss)*((Hp(ss)*sid1(i1)+Hc(ss)*sid2(i1)).*e0);
    %     figure
    %     plot(real(sim))
    %     grid
    %     xlabel('Time domain')
    %   whos sim
    sim=fft(sim).*NORM;
    %whos sim*
    if dsfact >1

      if ss==1
        sim0=zeros(1,piahead2.nsamples);
      end
      sim0(k1:k2)=sim0(k1:k2)+sim(k1int:k2int);
    else
      if ss==1
        simN=sim;
      else
        simN=simN+sim;
      end
    end

    %     figure
    %     semilogy(dnu,abs(sim(1:length(sim)/2)),'m');
    %     grid
    %     title('One Added signal')
  end
  if dsfact >1
    simN=sim0;
  end
  if nfigD <=nfig
    figure
    semilogy(dnu,abs(simN),'k')
    grid
    title('All added signals')
  end
