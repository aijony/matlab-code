function [PAR,simsour] = SETPARMS(many_simpar,N,piahead2);
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
for ss=1:N %constructs parameters of simulated signals 1:N
        
        simpar=many_simpar{ss};
        pars.sour=simpar{1};
        pars.ant=simpar{2};
        pars.amp=simpar{3};  
        simsour(ss)=simpar{1};
        antenna(ss)=simpar{2};
        simamp(ss)=simpar{3};
        eta(ss)=simsour(ss).eta;
        psi(ss)=simsour(ss).psi*pi/180;
        fi(ss)=2*psi(ss);
        Hp(ss)=sqrt(1/(1+eta(ss)^2))*(cos(2*psi(ss))-1j*eta(ss)*sin(2*psi(ss)));
        Hc(ss)=sqrt(1/(1+eta(ss)^2))*(sin(2*psi(ss))+1j*eta(ss)*cos(2*psi(ss))); 
    
        %display('simsour')
        simsour(ss);
        t0=piahead2.mjdtime;
        sour=new_posfr(simsour(ss),t0);
        f00=sour.f0;
        f0(ss)=f00;
        df0(ss)=sour.df0;
        ddf0(ss)=sour.ddf0;
        %Create PAR structure
        PAR(ss).f0=f0(ss);
        PAR(ss).df0=df0(ss);
        PAR(ss).ddf0=ddf0(ss);
        PAR(ss).Hp=Hp(ss);
        PAR(ss).Hc=Hc(ss);
        PAR(ss).t0=t0;
        PAR(ss).antenna=antenna(ss);
        PAR(ss).simamp=simamp(ss);
        
end

