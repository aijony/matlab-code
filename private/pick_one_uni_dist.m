function [out]=pick_one_uni_dist(min,max)

%reset(RandStream.getDefaultStream,sum(100*clock))

out=min+(max-min).*rand(1,1);
