function runstr=O2LL(k)
% run structure
%
%   k   particular sets

if ~exist('k','var')
    k=1;
end

%runstr.ini=5.772202053240740e+04;  %November 30, 2016 hh:mm:ss -> 002935 (read from 1st p10) %lalapps_tconvert 2016-11-30 00:29:35
%runstr.fin=5.799599997685185e+04;  %August 31, 2017 -> EXPECTED 
runstr.ini= 5.772263684027777e+04;  %November 30, 2016 hh:mm:ss -> %lalapps_tconvert 2016-11-30 15:17:03 = 1164554240 GPS (see Ornella's email from 8 Sep 2017)
runstr.fin= 5.799101016203704e+04;  %August 25, 2017 -> REAL -> %lalapps_tconvert Aug 26, 2017 00:14:38 UTC = 1187741696 GPS (see Ornella's email from 8 Sep 2017)
%=> 268.37 gg in totale e il mid time e' il giorno 134.19, quindi mjd2gps(5.772263684027777e+04+134.19)=1176148257 GPS = Thu Apr 13 19:50:39 GMT 2017
runstr.ant=ligol;
runstr.run=sprintf('O2LL_%02d',k);
DNU=16384;
RUN_DUR_s= (runstr.fin-runstr.ini)*86400; 
DMIN=1.0E-8;
DMAX=1.0E-9;
switch k
    case 1
        TFFT=8192;
        BAND=256;   %fmax 128
        SUBSAM=DNU/BAND;  %64
        runstr.capt='02 2017  [0-128] Hz';
        runstr.epoch=5.785682684027778e+04;  %mid time for O2: Thu Apr 13 19:50:39 GMT 2017 = 1176148257 GPS GPS
        runstr.st=1/BAND;
        runstr.fr.dnat=1/TFFT;
        runstr.sd.dnat=1/TFFT/RUN_DUR_s;  
        howmany=ceil(DMIN/runstr.sd.dnat);
        runstr.sd.min=-howmany*runstr.sd.dnat;
        runstr.sd.max=ceil(DMAX/runstr.sd.dnat)*runstr.sd.dnat;
        runstr.fft.len=TFFT*DNU/SUBSAM;   
        runstr.fft.n=ceil(RUN_DUR_s/TFFT*2);
        runstr.anaband=[10 127 1];
        case 2
        TFFT=4096;
        BAND=1024;   %2*fmax. fmax 512
        SUBSAM=DNU/BAND;  %16
        runstr.capt='O2 2017  [0-512] Hz';
        runstr.epoch=5.785682684027778e+04;  %mid time for O2: Thu Apr 13 19:50:39 GMT 2017 = 1176148257 GPS GPS
        runstr.st=1/BAND;
        runstr.fr.dnat=1/TFFT;
        runstr.sd.dnat=1/TFFT/RUN_DUR_s;
        howmany=ceil(DMIN/runstr.sd.dnat);
        runstr.sd.min=-howmany*runstr.sd.dnat;
        runstr.sd.max=ceil(DMAX/runstr.sd.dnat)*runstr.sd.dnat;
        runstr.fft.len=TFFT*DNU/SUBSAM;   
        runstr.fft.n=ceil(RUN_DUR_s/TFFT*2);
        runstr.anaband=[127 507 5]; 
        case 3
        TFFT=2048;
        BAND=2048;   %fmax 1024
        SUBSAM=DNU/BAND; %8
        runstr.capt='O2 2017  [0-1024] Hz';
        runstr.epoch=5.785682684027778e+04;  %mid time for O2: Thu Apr 13 19:50:39 GMT 2017 = 1176148257 GPS GPS
        runstr.st=1/BAND;
        runstr.fr.dnat=1/TFFT;
        runstr.sd.dnat=1/TFFT/RUN_DUR_s;  
        howmany=ceil(DMIN/runstr.sd.dnat);
        runstr.sd.min=-howmany*runstr.sd.dnat;
        runstr.sd.max=ceil(DMAX/runstr.sd.dnat)*runstr.sd.dnat;
        runstr.fft.len=TFFT*DNU/SUBSAM;   
        runstr.fft.n=ceil(RUN_DUR_s/TFFT*2);
        runstr.anaband=[509 1019 5];
        case 4
        TFFT=1024;
        BAND=4096;   %fmax 2048 
        SUBSAM=DNU/BAND;  %4
        runstr.capt='O2 2017  [0-2048] Hz';
        runstr.epoch=5.785682684027778e+04;  %mid time for O2: Thu Apr 13 19:50:39 GMT 2017 = 1176148257 GPS GPS
        runstr.st=1/BAND;
        runstr.fr.dnat=1/TFFT;
        runstr.sd.dnat=1/TFFT/RUN_DUR_s;
        howmany=ceil(DMIN/runstr.sd.dnat);
        runstr.sd.min=-howmany*runstr.sd.dnat;
        runstr.sd.max=ceil(DMAX/runstr.sd.dnat)*runstr.sd.dnat;
        runstr.fft.len=TFFT*DNU/SUBSAM;   
        runstr.fft.n=ceil(RUN_DUR_s/TFFT*2);
        runstr.anaband=[1023 2043 5];
   
end


runstr.hw_inj={};  %modify/correct


