function [PARS,simsour]=Createmodulatedsignals(piahead2,N,freq_band)

% N is the number of random signals that you want to inject into the data
% FACT1ONSQRT2=1/sqrt(2); % correct a factor due to the function gd2sfdbfile 
if ~exist('N','var')
    N=2;
end

if ~exist('freq_band','var')
   freq_band(1)=100;  %beginning Hz where to inject signals
   freq_band(2)=110;
end  

interval=(freq_band(2)-freq_band(1))/N;
for kk=1:N 
    minimum_f=freq_band(1)+(kk-1)*interval; %to check: we want to add signals in different freq bins. MODIFY accordingly to needs 
    maximum_f=freq_band(1)+kk*interval;     
    sourrrr=generate_rand_signalA(minimum_f,maximum_f);
    many_simpar{kk}={sourrrr,ligol,sourrrr.h/piahead2.einstein};
    many_simpar{kk}{1};
end

 [PARS,simsour]=SETPARMS(many_simpar,N,piahead2);
 simsour(1).f0;
