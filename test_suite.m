function test_suite(varargin)
  % Test and demo runner
  %
  % Run tests or demos in the tests/ folder
  % Compatible with both Matlab and GNU Octave
  %
  % Arguments can either be individual files (i.e. tests or demos) in
  % the test/ directory, or predefined sets of tests.
  %
  % Examples of individuals tests would be
  % > test_suite('testGraph') % to run mock graphs
  % > test_suite('mockSFDB') % to run mock SFDB test
  % > test_suite('inject') % to run injection demo
  %
  % All possible sets of tests are: 'aux', 'graphs', and 'demos'

  addpath("tests")

  % Test list applicable
  auxTests = {'mockSfdb', ...
              'mockSfdbBlock', ...
              'mockPeakmap', ...
              'testPeaks'};
  graphs = {'testGraph'};
  demos = {'inject', 'graphs'};

  % Default set of tests is default
  if(isempty(varargin))
    varargin{1} = "default";
  end

  % Specify which test(s)/demo(s) to run
  switch(varargin{1})
    case "default"
      % Use Matlab/Octave regular test runner
      t = [runtests("tests")]; %#ok<NBRAK>
      if(~isempty(t))
        disp(t);
      end
    case "aux"
      tests(auxTests);
    case "graph"
      tests(graphs);
    case "demo"
      tests(demos);
    case "all"
      test_suite("default");
      addpath("tests")
      tests([auxTests, demos]);
    otherwise
      tests(varargin);
  end

  rmpath("tests");

end

function tests(vars)
  % Helper function to run and time all tests/demos in a list
  disp(' ');
  tStart = tic;
  successes = cellfun(@(t) runTest(t), vars);
  tElapsed = toc(tStart);
  disp(' ');
  disp(['Results: ', int2str(sum(successes)) '/', ...
        int2str(length(vars)), ' tests succeeded in ', ...
        int2str(tElapsed), 's.' ]);
end

function success = runTest(t)
  % Helper function to run, time, and debug a single test/demo
  try
    tStart = tic;
    feval(t);
    success = 1;
    tElapsed = toc(tStart);
    disp([t, ' succeeded in ', num2str(tElapsed) 's']);
  catch err
    disp([t, ' failed with: ']);
    disp(err);
    success = 0;
  end
end
