function [fileF, fileP, snagLib] = config()
  % Edit this function to where to find data files.

  % Snag Lib
  snagLib='library/snag';

  % Change accordingly to your path
  datadir='SFDBandP10/';

  % All files with certain prefixes
  dirP = dir(strcat(datadir, '*.p10'));
  dirF = dir(strcat(datadir, '*.SFDB09'));

  % What is needed is a cell array of file strings
  % SFDB: inlcudes FFTs, AR estimation, and periodogram
  fileF = cellfun(@ (x) strcat(datadir, x), {dirF.name}, ...
                  'UniformOutput', false);

  % Peakmaps
  fileP = cellfun(@ (x) strcat(datadir, x), {dirP.name}, ...
                  'UniformOutput', false);

end
