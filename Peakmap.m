classdef Peakmap < handle
  % Peakmap (usually corresponding to SFDBBLOCK)

  properties

    % Datetime
    mjd;
    % Number of peaks
    npeak;
    % Undocumented
    vp; spini; spdf; splen; sp;
    % Offset Indices
    b;
    % Ratio Amplitude
    a;
    % Signal Peak Amplitude
    m;
    % Frequencies
    bb;

    % Original indices (starting from 0)
    loc;

    % Corresponding block's deltanu
    deltanu;
    % Corresponding block's normalization constant
    einstein;
    % Corresponding block's time
    mjdtime;

  end

  methods(Static)

    function obj = read(fid, dfr)
      % PEAKMAP class quasi constructor from a binary FID

      obj = Peakmap;

      % Read header information
      obj.mjd = fread(fid, 1, 'double');
      obj.npeak = fread(fid, 1, 'int32');

      obj.vp = fread(fid, 6, 'double');
      obj.spini = fread(fid, 1, 'double');
      obj.spdf = fread(fid, 1, 'double');
      obj.splen = fread(fid, 1, 'int32');
      try
        obj.sp = fread(fid, obj.splen, 'float32');
      catch
        disp("Warning: Peakmap.splen is invalid size, zeroing Peakmap.sp")
        obj.sp = zeros(1,obj.splen);
      end

      % Check if peaks
      if(obj.npeak > 0)
        obj.b = fread(fid, obj.npeak, 'int32');
        obj.a = fread(fid, obj.npeak, 'float');
        obj.m = fread(fid, obj.npeak, 'float');
      else
        % Zero out peak information
        obj.a = zeros(1,1);
        obj.b = obj.a;
        obj.m = obj.a;
      end

      % I believe these are the same
      obj.deltanu = dfr;

      % Convert indices to frequencies
      obj.bb = (obj.b - 1) * dfr;

    end

    function obj = create (sfdbBlock, threshold, f_begin, pow_spec, squareNorm)
      % PEAKMAP class quasi constructor that generates peaks
      %
      % Construct a PEAKMAP object by calculating local peaks above a
      % THRESHOLD. Also loads metadata from the data's SFDB entry.
      %
      %  * SFDBBLOCK : reference SFDB entry
      %  * THRESHOLD : minimum peak value (default = 2.5)
      %  * F_BEGIN : offset for the peak index (default = 0)
      %  * POW_SPEC : Optional power spectrum, otherwise generates one
      %  * SQUARENORM : Whether the POW_SPEC is squared and normalized
      %  (default: false)
      %
      % See: PEAKMAP.EXPORT

      obj = Peakmap;

      % Block information is kept
      obj.deltanu = sfdbBlock.header.deltanu;
      obj.einstein = sfdbBlock.header.einstein;
      % While redundant both are needed
      obj.mjdtime = sfdbBlock.header.mjdtime;
      obj.mjd(1:obj.npeak) = obj.mjdtime;

      if ~exist('threshold', 'var')
        threshold = 2.5;
      end
      if ~exist('f_begin', 'var')
        f_begin= 0;
      end
      if ~exist('pow_spec', 'var')
        % Generate non-normalized complex signal
        squareNorm = false;
        % Use default tau and maxage
        tau = 0.2;
        maxage = tau / obj.deltanu;
        % Regenerate power spectrum (not yet squared normalized)
        pow_spec = sfdbBlock.generate_pow_spec(tau, maxage, false);
      end
      if ~exist('squareNorm', 'var') || (squareNorm == false)
        % Relabel power spectrum to squared norm
        xamed = pow_spec;
        % Normalize and square
        pow_spec = (xamed * obj.einstein).^2;
      else
        % Estimate non-normalized and now real signal
        xamed = pow_spec.^(1/2) / obj.einstein;
      end

      % Squared normalized SFTs
      [data, ~] = sfdbBlock.data;

      % Ratio of the squared normalized SFTs and power spectrum
      ratio = data ./ pow_spec;

      % Generate peaks of ratios and their indices in the data
      [pks, loc] = Signal.peaks(ratio, threshold);
      obj.npeak = length(loc);
      obj.loc = loc;

      % Indices with given offset
      obj.b = f_begin + loc;
      % Ratio's peaks
      obj.a = pks;
      % SFT (unsqaured and unnormalized) peaks
      obj.m(1:obj.npeak) = xamed(loc);

      % Updated frequencies
      obj.bb = (obj.b - 1) * obj.deltanu;
    end

  end

  methods

    function MM = MM(obj, factor)
      % Calculate the normalized square of peak SFT values
      % Optionally normalize it to a FACTOR

      if ~exist('factor', 'var')
        factor = 10^-20;
      end

      MM = obj.m .* obj.m * factor^2;
    end

    function [peaks, ind, the_time] = export(obj)
      % Export PEAKMAP in a specified array format.

      peaks = zeros(5, obj.npeak);
      if (obj.npeak > 0)
        for i = 1:obj.npeak
          peaks(1, i) = obj.mjd(i);
          peaks(2, i) = obj.bb(i);
          peaks(3, i) = obj.a(i);
          peaks(4, i) = obj.m(i);
          peaks(5, i) = 0;
        end
      else
        peaks = [];
      end

      ind = obj.npeak;
      the_time = obj.mjdtime;
    end

  end

end
