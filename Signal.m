classdef Signal
  % Miscellaneous signal processing related functions

  methods(Static)

    function [x, t] = sin(f, fs, N, A, T, deltaf)
      % Sinusoidal signal generator
      %
      % Create a discrete sin wave coefficients X with time vector T.
      %
      %  - F : frequency (default 35 Hz)
      %  - FS : Sampling rate (default: F 10^2)
      %  - N : Number of cycles (default: 10)
      %  - T : Starting time (default: 0)
      %  - A : Amplitude
      %  - DELTAF : A function of original frequency and time that
      %  outputs a new frequency (default: g(f, x) = f)

      % Default values
      if ~exist('f', 'var')
        f = 25;
      end
      if ~exist('fs', 'var')
        % Hopefully  high enough resolution
        fs = f * 100;
      end
      if ~exist('N', 'var')
        N = 5 * fs / f;
      end
      if ~exist('A', 'var')
        A = 1;
      end
      if ~exist('T', 'var')
        T = 0;
      end
      if ~exist('deltaf', 'var')
        % Identity composed with first
        deltaf = @(f, t) f;
      end

      % Calculate discrete time vector
      t = (T:N - 1 + T) * 1/fs;

      % Calculate signal
      x = A * exp(1).^(2 * pi * deltaf(f, t) .* t .* sqrt(-1));

      % Bandlimit
      % B = abs(max(deltaf(f, x)));

    end

    function vec = index(x, fs)
      % Return index vector based off of amount of data X and sampling
      % rate FS.

      N = length(x);
      vec = (0:N - 1) * 1/fs;

    end

    function [x, fs] = fft_hann(x, fs)
      % Return a FFT Signal X and with correct sample rate FS
      %
      % Uses a Hanning window to FFT the original signal.

      N = length(x);
      fs = N/fs;

      % Hanning window
      hann = 0.5 - 0.5 * cos(2 * pi * linspace(0,1,N));

      x = fft(x .* hann);
    end

    function [pks, loc] = peaks(x, threshold)
      % Select peaks from signal by shifting the indices
      % With values as PKS and indices as LOC
      % Finds local maxima of a signal X above a threshold THRESHOLD

      % It is doubtful there is a faster peakfinding algorithm without
      % calling out to C/C++.

      x_cut = x(2:end-1); % Non-shifted signal
      x_lshift = x(1:end-2); % Left shift signal by 1
      x_rshift = x(3:end); % Right shift signal by 1

      % Started from second element so add 1
      % Then find all elements that match criteria
      loc = 1 + find(x_cut > threshold & ... % Certain threshold
                     x_cut > x_lshift & x_cut > x_rshift); % Maxima

      pks = x(loc);
    end

    function y = wgn(m, n, p, imp, seed)
      % Generate white noise to a MxN matrix with power P and
      % impedance IMP (default 1) given a seed optional SEED.
      if isempty (imp)
        imp = 1;
      end
      if ~isempty (imp)
        randn ("state", seed);
      end
      y = (sqrt (imp*p)) * randn (m, n);
    end

    function plotfs(x, fs, name)
      % Plot Signal X based off of sample-rate FS and label with
      % optional NAME.

      if ~exist('name', 'var')
        name = "Signal";
      end

      figure;
      plot(Signal.index(x, fs), x)
      title(name)
      xlabel('Domain');
      ylabel('Amplitude');

    end

    function ar = ar_naive(x, p, w, norm)
      % Simple AR(P) model that does not account for age. This was
      % just practice and is most assuredly incorrect. However, it
      % does provide a fast replacement for making mock programs.
      %
      % An order P autoregressive model on data X.
      % Optional decay coefficient of W and normalize answer by NORM

      if ~exist('norm', 'var')
        w = e^-1;
      end
      if ~exist('norm', 'var')
        norm = 1;
      end

      k = 0:p;
      k_i = @(i) k(find(i > k));
      y_i = @(i) sum(x(i-k_i(i)) .* w.^k_i(i)) ./ sum(w.^k_i(i));
      y = arrayfun(y_i, 1:length(x));

      ar = y .* norm;

    end

    function [x, fs] = sample_naive(x, fs, ratio)
      % Naïvely Sampled signal
      % This was just practice and is most assuredly incorrect.
      %
      % Provided a RATIO sample the signal, otherwise attempt
      % losslessly sample the signal by selecting peaks from FFTs and
      % using Nyquist-Shannon Theorem.
      %
      % Built in sampling algorithms should be used instead -
      % see RESAMPLE.

      if ~exist('ratio', 'var')
        % Check if bandlimit exists
        % Estimate possible bandlimit
        ft = abs(fft(x));
        pks = Signal.peaks(ft);
        % Left median
        B = pks(ceil(length(pks) / 2)); %#ok<*PROPLC>
      end
      % Nyquist-Shannon Theorem
      fsNew = 2 * B;

      % How many samples to skip
      % Add a factor of two for good measure
      ratio = ceil(fs / fsNew / 2);

      % Keep track of which indices are sampled
      pos = [];

      % Go through entire set
      for j = 1:length(x)
        if mod(j, ratio) == 1
          % Record original index
          pos(end+1) = j;
        end
      end

      % Remove any preallocation in domain, and only selected sampled
      % indices - pass on sample-rate and bandlimit.
      x = x(pos);
      fs = fs/ratio;

    end

  end

end
