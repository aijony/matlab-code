classdef SfdbBlock < handle
  % A SFT of an SFDB

  properties

    % Metadeta for SFT block
    header;
    %  * SPS : short power spectrum
    %  * SFT : short Fourier transforms

    % Time averages
    tps;
    % Short power spectrum
    sps;
    % Short Fourier transforms
    sft;

    % Normalizing factor to go from FFTs to spectra.
    normTOT;
    % Auxiliary normalizing factor for data and windowing
    norm;

  end

  properties (Access = private)

    dnu_data;
    dnu_spectra;
    dnu_ARspectra;

  end

  methods(Static)

    function obj = read (fid)
      % SFDBBLOCK Class Quasi Constructor from a binary FID

      [header, tps, sps, sft] = pia_read_block_09(fid);

      obj = SfdbBlock(header, transpose(tps), ...
                      transpose(sps), ...
                      transpose(sft));
    end

  end

  methods

    function obj = SfdbBlock (header, tps, sps, sft)
      % SFDBBLOCK Class Constructor
      %
      % Sets the parameters to properties then calculates and stores
      % the normalization factors.
      %
      %  * HEADER : metadata
      %  * TPS : time averages
      %  * SPS : short power spectrum
      %  * SFT : short Fourier transforms
      %

      obj.header = header;
      obj.tps = tps;
      obj.sps = sps;
      obj.sft = sft;

      %{

       NORM relies on normd and normw. Specifically normd accounts for
       the data and normw accounts for the windowing

       Sometimes blocks are generated without these norms
       (for tests or eof blocks)
      %}
      if (isfield(obj.header, 'normd') && ...
          isfield(obj.header, 'normw'))

        obj.norm = sqrt(2) * obj.header.normd * obj.header.normw;

        % Prevent divide by zero error
        if (~isfield(obj.header, 'spare4') || obj.header.spare4 == 1)
          spare = 1;
        else
          spare = sqrt(1 - obj.header.spare4);
        end

        obj.normTOT = obj.header.einstein^2 * (obj.norm / spare)^2;
      else
        % norm = normTOT = 1
        [obj.norm, obj.normTOT] = deal(1);
      end

    end

    function [data, dnu] = data (obj)
      % Data and delta-nu
      %
      % DATA is squared modulus of the short Fourier Transforms,
      % normalized to represent a spectrum

      data = abs(obj.sft) .* abs(obj.sft) * obj.normTOT;

      xdata = 0:length(data) - 1;
      dnu = xdata * obj.header.deltanu; %frequencies

      obj.dnu_data =  dnu;

    end

    function [periodogram, dnu] = periodogram (obj)
      % Periodogram and delta-nu
      %
      % The result of squaring the product of
      % the Einstein normalization factor (10^20) and the time
      % averages

      periodogram = obj.tps .* obj.tps * obj.header.einstein^2;

      xspectra = 0:length(periodogram) - 1;
      dnu = obj.header.deltanu * obj.header.red * xspectra;

      obj.dnu_spectra = dnu;

    end

    function [ARspectrum, dnu] = ARspectrum (obj)
      % Autoregressive spectrum and delta-nu
      %
      % The result of squaring the product of the
      % Einstein normalization factor (10^20) and the short power
      % spectrum.

      ARspectrum = obj.sps .* obj.sps * obj.header.einstein^2;

      xspectra = 0:length(ARspectrum) - 1;
      dnu = obj.header.deltanu * obj.header.red * xspectra;

      obj.dnu_ARspectra = dnu;

    end

    function pow_spec = generate_pow_spec (obj, tau, maxage, normSquared)
      % GENERATE_POW_SPEC re-estimates the power spectrum based off
      % of the data in the complex, non-squared, non-normalized SFT.
      %
      % See: USE_REESTIMATE_POW_SPEC

      if ~exist('tau', 'var')
        tau = 0.02; %0.02 Hz
      end
      if ~exist('maxage', 'var')
        maxage = tau / obj.header.deltanu; %10 samples
      end
      if ~exist('normSquared', 'var')
        normSquared = true;
      end

      xamed = USE_reestimate_pow_spec(obj.sft, ...
                                      obj.header.deltanu, ...
                                      obj.header.normd, ...
                                      obj.header.normw, ...
                                      tau, maxage);

      if normSquared
        pow_spec = (xamed * obj.header.einstein).^2;
      else
        pow_spec = xamed;
      end

    end

    function inject(obj, x, amp)
      % Inject the FFT of signal X into the SFT of SFDBBLOCK
      %
      % Uses a Hanning window before applying the FFT with a amplitude
      % of AMP.

      if ~exist('amp', 'var')
        amp = 1;
      end
      % Hanning Filter
      hann = 0.5 - 0.5 * cos(2 * pi * linspace(0,1,length(x)));

      % Inject FFT of Hanning Filtered input at certain amplitude
      inj = fft(x .* hann * amp);
      obj.sft = obj.sft + inj;

    end

  end

end
