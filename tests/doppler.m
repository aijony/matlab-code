%!demo doppler
%!test

addpath("..")
[fileF, fileP, snagLib] = config();

% Snag is required for this demo
addpath(genpath(snagLib));

%% Create Sfdb

n = 50;

sfts = Sfdb.read(fileF, fileP, n);

% sfts = mockSfdb(true, n);

nInjections = 1;
sfts.inject_doppler(100,110,nInjections);

sfts.peakmap = sfts.create_peakmap();

[pm, f] = sfts.correct_doppler(1);

figure
title(num2str(f))
xlabel ("Count");
ylabel ("Frequency (Hz)");
histogram([pm{1:n}], 1000);


hold on;
figure

[pm, f] = sfts.correct_doppler(2);
histogram([pm{1:n}], 1000);
title("Doppler corrected signal for 50 SFTs w/ f0 = 107.448")
ylabel ("Counted Peaks");
xlabel ("Frequency (Hz)");





