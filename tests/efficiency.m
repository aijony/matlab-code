%!demo efficiency
%!test

addpath("..")
[fileF, fileP] = config();

%% Create Sfdb

n = 15;

sfts_base = Sfdb.read(fileF, fileP, n);
% Save memory
sfts_base.peakmap = [];

% sfts = mockSfdb(true, n);

nfft = sfts_base.block_count;

% Frequencies to inject
f = 10:20;
% Variables
tau = 2 * 10.^(-4:.5:1);
%amp = .01:.1:1;

% Efficiency
eff = ones(1,length(tau));
% Max EFF for all F
mx = ones(1,length(tau));
% Min EFF for all F
mn = ones(1,length(tau));

% Load parallel package if Octave
if ~exist('parfor')
  pkg load parallel;
end

%for i = 1:length(amp)
% Parallel version
parfor i = 1:length(tau)
  rate = sfts_base.efficiency(f, 1.361e-2, tau(i));

  % Calculate average efficiency and min and max values
  eff(i) = mean(rate);
  mx(i) = abs(quantile(rate,.75) - eff(i));
  mn(i) = abs(quantile(rate,.25) - eff(i));
end

%% Graphcl
h = figure;
hold on;
errorbar(tau, eff, mn, mx, '-o');
% plot(amp * 10^-20, eff, '-o');
set(gca,'xscale','log')

title('Efficiency of Injections (10-20Hz, h=1.361e-22)')
xlabel ("\tau");
ylabel ("\eta");
hold off;

% saveas(h,'tau1','fig');
% saveas(h,'tau1','jpg');
