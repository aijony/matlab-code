function sfdbBlock = mockSfdbBlock(regen, t)
  % Creates a persistent mock SFDBBLOCK object and will reuse the same
  % block every call unless optional argument REGEN is
  % true. Optionally provide a time vector T.
  %
  % Signal generated is a sin wave with increasing frequency
  % superimposed with seeded whitenoise.

  persistent mockSfdbBlock;

  if exist('regen', 'var') && ...
     regen == true || ...
     ~isobject(mockSfdbBlock)
    if ~exist('t', 'var')
      t = 0:.1:1;
    end

    seed = 42 * t(end);

    % Try built-in white noise
    try
      % Attempt to load package
      if ~exist('wgn')
        pkg load communications;
      end
      x = wgn (1, 2*length(t), .01, .01, seed);
    catch
      % Use pre-packaged
      x = Signal.wgn(1, 2*length(t), .01, .01, seed);
    end

    sft = fft(x);
    deltanu = (t(2) - t(1))*length(t)/2;

    header = {};
    header.normd = 1;
    header.normw = 1;
    header.spare4 = 0;
    header.einstein = 10^-20;
    header.deltanu = deltanu;
    header.red = 1;
    header.eof = 0;
    header.mjdtime = t(1);

    % Needed for doppler injection and correction
    header.tsamplu = t(end) - t(1);
    header.nsamples = length(t);

    header.vx_eq = 1;
    header.vy_eq = 1;
    header.vz_eq = 1;

    header.px_eq = 1;
    header.py_eq = 1;
    header.pz_eq = 1;

    header.check_ps_lf = 0;

    % Create a temporary block to run object methods with
    blockTmp = SfdbBlock(header,0,0,sft);
    % Generate power spectrum
    sps = blockTmp.generate_pow_spec(0.02, 0.02/header.deltanu, false);

    % Create a periodogram if function exists
    try
      % Attempt to load signal package
      if ~exist('periodogram')
        pkg load signal;
      end
      tps = periodogram(x);
    catch
      % Don't even bother with periodogram
      tps = 0;
    end

    % Return new object
    mockSfdbBlock = SfdbBlock(header, tps, sps, sft);
  end
  sfdbBlock = mockSfdbBlock;

end

%!test
%! assert(isobject(mockSfdbBlock(true)))
