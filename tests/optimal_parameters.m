%!demo optimal_parameters
%!test

addpath("..")
[fileF, fileP] = config();

%% Create Sfdb

n = 100;

sfts = Sfdb.read(fileF, fileP, n);
% Save memory
sfts.peakmap = [];

nfft = sfts.block_count;

f = 10:1024;
tau0 = 0.02;
maxage0 = tau0/sfts.block{1}.header.deltanu;

options = optimset('Display','iter');

[x, fval] = fminsearch (@(x) 1/mean(sfts.efficiency(f,10e-5,x(1),x(2))) ...
                        , [tau0;maxage0], options);

disp("DONE");
disp("Args:");
disp(x);
disp("Value:");
disp(fval);
