%!test testGraph; close all;

function testGraph()
  % Visualize MOCKSFDB using graphs

  sfdb = mockSfdb(true, 2);
  pm = sfdb.peakmap{1};
  block = sfdb.block{1};

  % Graph SFDB data
  Graph.Square_Periodogram(block)
  Graph.AR_and_NormFFTSquared(block)
  Graph.Peaks_to_AvgNoise(pm)
  Graph.Peaks_PowSpec_and_FFT(block, pm)
  Graph.Squared_Peaks_PowSpec(block, pm)
  Graph.Mean_Noise(pm)
  Graph.Compare_Spectrums(block)

  pow_spec = block.generate_pow_spec();
  gen_pm = Peakmap.create(block, 2.5, 0, pow_spec, true);
  Graph.Squared_Peaks_PowSpec(block, gen_pm, pow_spec);

end
