function sfdb = mockSfdb(regen, entries)
  % Creates a persistent mock SFDB object with MOCKSFDBBLOCKs and
  % MOCKPEAKMAPs, and will reuse the same SFDB every call unless
  % optional argument REGEN is true.
  %
  % Specify a certain number of ENTRIES


  persistent mockSfdb;

  if exist('regen', 'var') && ...
     regen == true || ...
     ~isobject(mockSfdb)

    if ~exist('entries','var')
      entries = 2;
    end
    sfdb = Sfdb();
    sfdb.nfft = entries;

    % Set sample-rate
    fs = 10e2;

    % Each i is a unit of time as well as an index
    for i = 1:entries
      % Create a block based off of a sample-rate over a time-range
      sfdb.block{i} = mockSfdbBlock(true, i:1/fs:(i+1));
      % Uses persistent mockSfdbBlock to generate Peakmap
      sfdb.peakmap{i} = mockPeakmap();
    end

    mockSfdb = sfdb;
  end

  sfdb = mockSfdb;

end

%!test
%! assert(isobject(mockSfdb(true)))
