%!demo inject
%!test

%% Demos injecting over an entire SFDB

addpath("..")

% You can run this on real data but it takes a while...
% [fileF, fileP] = config();

% n = 72;
% sfts = Sfdb.read(fileF, fileP, n);
% deltaf = @(f, t) f .* (1 - 10e-7 * t.^1.01);
% % Save memory
% sfts.peakmap = [];
% % Wonky data
% sfts.block = [sfts.block(1:15), sfts.block(35:61), sfts.block(63:n)];

sfts = mockSfdb(true, 1000);
deltaf = @(f, t) f .* (1 - 10e-12 * t.^1.01);


nfft = sfts.block_count;
N = length(sfts.block{1}.sft);
[~, dnu] = sfts.block{1}.data;

f = 800;
sfts.inject(f, 1, deltaf);
out = zeros(N,nfft);
% t = zeros(nfft);
peakmaps = sfts.create_peakmap(1);

for i = 1:nfft
  loc = peakmaps{i}.loc;
  [data, ~] = sfts.block{i}.data();
  % Cleanup memory
  [sfts.block{i}.sps, sfts.block{i}.tps, sfts.block{i}.sft] = deal([]);
  out(loc,i) = data(loc);
end

%outx = cellfun(@(pm) pm.bb, peakmaps, 'UniformOutput', false);
%outy = cellfun(@(pm) pm.MM, peakmaps, 'UniformOutput', false);
%outS = cellfun(@(pm) pm.a, peakmaps, 'UniformOutput', false);

%figure;
%for i = 1:nfft
% scatter(i * ones(size(outx{i})),outx{i}, [], log10(outy{i}));
% hold on;
%end
%hold off;
%xlabel ("Time")
%ylabel ("Frequency")

imagesc(1:nfft, dnu, log10(abs(out).^2));
set(gca,'YDir','normal');
xlabel ("SFT Index")
ylabel ("Frequency")
title('Injected Signal (mock data)')
