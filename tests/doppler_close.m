%!demo detect
%!test

%% Detects signals processed

addpath("..")
[fileF, fileP, snagLib] = config();

% Snag is required for this demo
addpath(genpath(snagLib));

%% Create Sfdb
% Number of SFTs to read
n = 15;

sfts = Sfdb.read(fileF, fileP, n);
% Memoize (This is average peaks)
sfts.avg_count = sfts.avg_peak_count;
% Save memory
sfts.peakmap = [];

% sfts = mockSfdb(true, n);
deltanu = sfts.block{1}.header.deltanu;
dnus = (-4:5) * deltanu;
amp = 10^(-24);

%% Inject and detect
nfft = sfts.block_count;

% Frequencies to inject
f0 = [50,175,300,425,550,675,800,925];
% Create array of repeated f0
f1 = (ones(length(f0), length(dnus)) .* transpose(f0));

% Spacing between injected signals
% (Every nth frequency bin)
n = [0,1,2,5,10];

% Add a certain spacing of deltanus (starting from 0)
for i = 1:length(n)
  f{i} = f1 + dnus * n(i);
end

% Needs to be a constant for parfor
lf = length(f);
% Pre-allocate arrray
rate = ones(lf,length(f0));

for i = 1:length(f0)
  for j = 1:lf
    % Efficiency of signal w/ 10^-24 amplitude
    rate(j,i) = sfts.efficiency_doppler(f{j}(i,:), 10^20 * amp);
  end
end

tot = [rate(:)]; %#ok<NBRAK>

%% Graph
fig = figure;
hold on;

bar(f0, rate);

yline(mean(tot),'-');

% Label manually
legend('N=1', 'N=2', 'N=5', 'N=10', '\mu');

title('Efficiency of 10 Injections Every Nth Frequency Bin')
xlabel ("f (Hz)");
ylabel ("\eta");
hold off;

% saveas(fig,'far_bins','fig')
% saveas(fig,'far_bins','jpg')

disp('Far bins (mean, sd):');
disp(mean(tot));
disp(std(tot));
