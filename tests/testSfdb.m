%!test testSfdb

%% Run tests using mock SFDB

addpath("..")

% Generaet SFDB
sfts = mockSfdb(true, 6);

% Check correct amount made
assert(isobject(sfts.peakmap{5}))
assert(isobject(sfts.block{5}))

% Check block vary over time
assert(prod(sfts.block{1}.sft == sfts.block{1}.sft) == 1)
assert(prod(sfts.block{1}.sft ~= sfts.block{5}.sft) == 1)

pm = sfts.peakmap{1};

% Check peakmap operations don't error
pm.bb; pm.MM;

block = sfts.block{1};

% Check block operations don't error
block.data;
block.periodogram;

% Regenerate ARspectrum
[ARspectrum, dnu] = block.ARspectrum;
pow_spec = block.generate_pow_spec();

% Round-about way of getting past floating point errors
% TODO: This may actually be bug
%       (i.e. pow_spec - ARspectrum ~ 10^-58)
assert(mean(pow_spec ./ ARspectrum) == 1);
