function peakmap = mockPeakmap(regen)
  % Creates a persistent mock PEAKMAP object based off of the a
  % previously MOCKSFDBBLOCK, and will reuse the same peakmap every
  % call unless optional argument REGEN is true.

  persistent mockPeakmap;
  if exist('regen', 'var') && ...
     regen == true || ...
     ~isobject(mockPeakmap)

    % Uses the previously generated mockSfdbBlock
    sfdbBlock = mockSfdbBlock();
    mockPeakmap = Peakmap.create(sfdbBlock);
  end

  peakmap = mockPeakmap;

end

%!test
%! assert(isobject(mockPeakmap(true)))
