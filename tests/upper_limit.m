%!demo upper_limit
%!test

addpath("..")
[fileF, fileP, snagLib] = config();

% Snag is required for this demo
addpath(genpath(snagLib));

%% Create Sfdb

n = 20;
sfts = Sfdb.read(fileF, fileP, n);

% Save memory
sfts.peakmap = [];

nfft = sfts.block_count;

f = 10:1024;

% Load parallel package if Octave
if ~exist('parfor')
  pkg load parallel;
end

x = ones(1,length(f));
fval = ones(1,length(f));

options = optimset('TolFun',1e-7,'TolX',1, ...
                   'Display','iter');

parfor i = 1:length(f)
  % I apologize to anyone reading this
  % The bulk of it is that we want to solve efficiency to be .95.
  % However, there are some complicated hacks to get fminbnd to solve
  % it correctly.
  %
  % Essentially we have to
  %   - Give the parameters inverted because we want to start from the
  %   lowest, but fminbnd starts from the highest
  %   - Use the absolute value so it doesn't pick a low amplitude
  %   - Give a concept of flow so that the solver knows which
  %   direction to go
  %   - However when we get to eff - .95 = .05 we have gone to far and
  %   give a negative flow.
  [x(i), fval(i)] ...
  = fminbnd ...
      (@(amp) ...
        feval ...
        (@(res) ...
          (- (res < 0) * (10^-8 * amp) ...
           - (abs(res - 0.05) < 10e-9) * ((10^-5 / amp) - 1) ...
          ) * (res ~= 0) + abs(res) ...
         , mean(sfts.efficiency(f(i), abs(amp / 1e7))) -.95) ...
       , 10e1, 10e7, options);
  x(i) = abs(x(i)) * 1e-27
end

% csvwrite('normal.csv', x)
scatter(f, x,'filled'); set(gca,'yscale','log')
