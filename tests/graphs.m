%!demo graphs
%!test

%% Demos all the possible graphs

addpath("..")

[fileF, fileP] = config();

sfts = Sfdb.read(fileF, fileP, 1);

pm = sfts.peakmap{1};
block = sfts.block{1};

% Graph SFDB data
Graph.Square_Periodogram(block);
Graph.AR_and_NormFFTSquared(block);
Graph.Peaks_to_AvgNoise(pm);
Graph.Peaks_PowSpec_and_FFT(block, pm);
Graph.Squared_Peaks_PowSpec(block, pm);
Graph.Mean_Noise(pm);
Graph.Compare_Spectrums(block);

pow_spec = block.generate_pow_spec();
gen_pm = Peakmap.create(block, 2.5, 0, pow_spec, true);
Graph.Squared_Peaks_PowSpec(block, gen_pm, pow_spec);
