%!test testPeakmap

% Check Peakmap.create.export against create_peakmap

addpath("..");

sfts = mockSfdb(true, 3);
pm = sfts.peakmap{2};
block = sfts.block{2};

tau = 0.2;
maxage = tau / block.header.deltanu;

xamed = block.generate_pow_spec(tau, maxage, false);

pm_gen = Peakmap.create(block, 0, 0, xamed, false);
[peakss1, ind1, the_time1] = pm_gen.export;

[data, ~] = block.data;

xamed = xamed * block.header.einstein;
[peakss2, ind2, the_time2] = create_peakmaps(xamed, data, 0, block.header);

assert(isequal(the_time1, the_time2));
assert(isequal(ind1, ind2));

% Close enough due to decimal precision
assert(sum(prod(peakss1 .* peakss2)) == 0);
