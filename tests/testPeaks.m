%!test testPeaks

function testPeaks()
  % Test various peak finding algorithms

  persistent count;
  if isempty(count)
    count = 50;
  elseif count <= 0
    error("Exceeded recursion")
  end

  addpath("..");

  % Time vector
  t = 2*pi*linspace(0,1,1024);
  % Create lots of peaks
  data = sin(pi * t) + 0.5*cos(6.09*t) + 0.1*sin(10.11*t+1/6) + 0.1 *sin(16.3*t+1/3);
  data = abs(data);

  assert(isvector(data));

  % Most of the peaks
  threshold = 0.5;

  peaks0Start = tic;
  [pks0, loc0] = Signal.peaks(data, threshold);
  time0 = toc(peaks0Start);

  peaks1Start = tic;
  [pks1, loc1] = testPeaks1(data, threshold);
  time1 = toc(peaks1Start);

  peaks2Start = tic;
  [pks2, loc2] = testPeaks2(data, threshold);
  time2 = toc(peaks2Start);

  try
    % Attempt to load signal package
    if ~exist('findpeaks')
      pkg load signal;
    end
    peaks3Start = tic;
    [pks3, loc3] = testPeaks3(data, threshold);
    time3 = toc(peaks3Start);

  catch
    % Don't want test to fail however
    [pks3, loc3] = deal(pks2, loc2);
    time3 = time2;
  end

  % Make sure everything has same results
  assert(isequal(pks0, pks1));
  assert(isequal(pks1, pks2));
  assert(isequal(pks2, pks3));

  assert(isequal(loc0, loc1));
  assert(isequal(loc1, loc2));
  assert(isequal(loc2, loc3));

  % Check speeds
  try

    % Check if signal was loaded
    if time2 ~= time3
      % Make sure 1 is fast
      assert(time1 < time3);
    end

    assert(time0 < time3); % Should still be faster
    assert(time0 < time2);

    assert(time1 < time2); % Faster vectorized code
    assert(time2 >= time3); % May be equal
  catch
    % Should eventually happen
    testPeaks;
  end

end

function [pks, loc] =  testPeaks1(x, threshold)
  % Select peaks from signal by shifting the indices
  % With values as PKS and indices as LOC
  %
  % Finds local maxima of a signal X above a threshol

  x_cut = x(2:end-1); % Non-shifted signal
  x_lshift = x(1:end-2); % Left shift signal by 1
  x_rshift = x(3:end); % Right shift signal by 1

  % Stared from second element so add 1
  % Then find all elements that match criteria
  loc = 1 + find(x_cut > threshold & ... % Certain threshold
                 x_cut > x_lshift & x_cut > x_rshift); % Maxima
  pks = x(loc);
end

function [pks, loc] = testPeaks2(x, threshold)
  % Select PEAKS by iterating over every element and comparing
  % neighbors with values as PKS and indices as LOC
  %
  % Finds local maxima of a signal X above a THRESHOLD
  %
  % Derived from create_peakmaps.m

  % Index of peak.
  % We technically might not find any peaks,
  % so start at zero
  n = 0;

  % First and last elements cannot be peaks
  % However, loop through the rest
  for i = 2:(length(x) - 1)
    % Only allow local peaks within a threshold
    if (x(i) > threshold && ... % x within threshold
        x(i) > x(i + 1) && ... % Check for left maxima
        x(i) > x(i - 1)) % Check for right maxima
      % Found a peak

      % Increment number of peaks
      n = n + 1;

      % Add as peak
      pks(n) = x(i);

      % Add index
      loc(n) = i;
    end
  end
end

function [pks, loc] = testPeaks3(x, threshold)
  % Use library code to run peaks if available
  %
  % I'm sure they did it better

  % Only work if findpeaks function exists

  [pks, loc] = findpeaks(x, "MinPeakHeight", threshold);

end
