function [ peakss,ind,the_time ] = create_peakmaps( xamed, data, thresh, piahead2)
  %sps is the AR spectrum
  %sft the FFT (complex and not normalized)

  if ~exist('thresh','var')
    thresh=2.5
  end

  header_parms=piahead2;
  f_begin=0;

  ratio=data./xamed.^2;

  peaks_index=0; %index for peaks
  for i=2:(length(ratio)-1)%1:length(ratio)
    if ratio(i)>thresh %for all ratios calculated, check if it's first bigger than threshold
      if ratio(i)>ratio(i+1)
        if ratio(i)>ratio(i-1) %check in middle of FFT, if bigger than both the one before and one after
          peaks_index=peaks_index+1;

          peakss(1,peaks_index)=header_parms.mjdtime;
          peakss(2,peaks_index)=f_begin+header_parms.deltanu*(i-1); %adding frequency from beginning
          peakss(3,peaks_index)=ratio(i);

          % Probably shouldn't be squared or normalized
          peakss(4,peaks_index)=xamed(i) / header_parms.einstein;
          peakss(5,peaks_index)=0;

        end
      end
    end
    %if none of these are true, i = i+1, the loop advances
  end

  if ~exist('peakss','var')
    disp('no peaks found at this time:')
    header_parms.mjdtime;
    peakss=[];
    %    det_vel_pos=[];
  end

  ind=peaks_index;
  the_time=header_parms.mjdtime;
  %peaks(1,:)  time repeated
  %peaks(2,:) all frequencies
end

%!test testPeakmap
