classdef Graph

  methods(Static)

    function Square_Periodogram(sfdbBlock)
      % Plot the periodogram^2

      [data, dnu] = sfdbBlock.data;
      [periodogram, dnu_spectra] = sfdbBlock.periodogram;

      figure;
      semilogy(dnu, data, 'b');
      hold on;
      semilogy(dnu_spectra, periodogram, 'k');
      grid;
      title('Periodogram');
      xlabel('Frequency');
      ylabel('Squared periodogram');
      hold off;

    end

    function AR_and_NormFFTSquared(sfdbBlock)
      % Plot Auto Regression with normalized FFT^2

      [data, dnu] = sfdbBlock.data;
      [ARspectrum, dnu_spectra] = sfdbBlock.ARspectrum;

      figure;
      semilogy(dnu, data, 'b');
      hold on;
      semilogy(dnu_spectra, ARspectrum, 'r');
      grid;
      title('AR and normalized FFT^2');
      xlabel('Frequency');
      ylabel('Squared AR spectrum (red). FFT (blue)');
      hold off;

    end

    function Peaks_to_AvgNoise(peakmap)
      % Plot Peaks to Average Noise Ratio

      [bb, a] = deal(peakmap.bb, peakmap.a);

      figure;
      plot(bb,a,'*');
      xlabel('Frequency');
      ylabel('Ratio');
      title('From the peakmap: ratio of peaks to average noise');
      grid;

    end

    function Peaks_PowSpec_and_FFT(sfdbBlock, peakmap)
      % Plot the Normalized Squared FFT, AR spectrum, and Peaks ratio

      [data, dnu] = sfdbBlock.data;
      [ARspectrum, dnu_spectra] = sfdbBlock.ARspectrum;

      [bb, a, MM] = deal(peakmap.bb, peakmap.a, peakmap.MM);

      figure;
      semilogy(dnu, data, 'b');
      hold on;
      semilogy(dnu_spectra, ARspectrum, 'k');
      grid;
      title('FFT (blue), AR (black) and selected peaks (red)');
      xlabel('Frequency');
      ylabel('Squared FFT, AR spectrum and PEAKS ratio normalized');
      semilogy(bb, MM, 'or');
      hold off;

    end

    function Squared_Peaks_PowSpec(sfdbBlock, peakmap, pow_spec, dnu_spectra)
      % Plot the Normalized Squared FFT, AR spectrum, and Peaks ratio

      [data, dnu] = sfdbBlock.data;
      if ~exist('pow_spec', 'var')
        [ARspectrum, dnu_spectra] = sfdbBlock.ARspectrum;
      else
        ARspectrum = pow_spec;
        if ~exist('dnu_spectra', 'var')
          dnu_spectra = dnu;
        end
      end

      Mnorm = median(ARspectrum);

      [bb, a, MM] = deal(peakmap.bb, peakmap.a, peakmap.MM);

      % (a m 10^-20)^2
      sele_peaks =  a .* a .*  MM;

      figure;
      semilogy(dnu,data, 'b');
      hold on;
      semilogy(dnu_spectra, ARspectrum, 'k');
      grid;
      title('FFT, AR and selected peaks');
      xlabel('Frequency');
      ylabel('Squared AR spectrum and PEAKS');
      semilogy(bb, sele_peaks, 'or');
      hold off;

    end

    function Mean_Noise(peakmap)
      % Plot the mean noise of peakmap

      [bb, m] = deal(peakmap.bb, peakmap.m);

      figure;
      plot(bb, m, '.');
      xlabel('Frequency');
      ylabel('Noise Mean');
      grid;

    end

    function Compare_Spectrums(sfdbBlock, tau, maxage)
      % Compare the new estimation of AR spectrum (done with the new
      % function and the one from SFDBBLOCK
      %
      % Uses TAU (default: 0.02 Hz) to compute MAXAGE if not given

      if ~exist('tau', 'var')
        tau = 0.02; % 0.02 Hz
      end
      if ~exist('maxage', 'var')
        maxage = tau / sfdbBlock.header.deltanu; % 10 samples
      end

      [~, dnu] = sfdbBlock.data;
      [ARspectrum, dnu_spectra] = sfdbBlock.ARspectrum;
      xamed = sfdbBlock.generate_pow_spec(tau, maxage, true);

      figure;
      semilogy(dnu, xamed, '.r');
      hold on;
      semilogy(dnu_spectra, ARspectrum, '.k');
      grid;
      title('Comparison: New (red) and old (black) AR spectrum');
      hold off;

    end

  end

end
