classdef Sfdb < handle
  % An object containing arrays of SFDB Blocks and Peakmaps
  properties

    % SFDB blocks (see: SFDBBLOCK)
    block;
    % SFDB block file(s) accessed
    block_file;

    % Peakmaps (see: PEAKMAP)
    peakmap;
    % Peakmak file(s) accessed
    peakmap_file;

    % Peakmap metadeta
    nfft;
    sfr;
    lfft2;
    dfr;
    inifr;

    % Injection metadata
    simsour;

    % Memoize
    avg_count;

  end

  methods(Static)

    function obj = read (block_file, peakmap_file, nreads)
      % SFDB Class Quasi Constructor
      %
      % Returns an SFDB object of read SFDBBLOCKs or PEAKMAPs
      % Takes a cell of strings of file paths.
      %
      % If specified, NREADS limits how many peakmaps/SFDB blocks to
      % read. For NREAD=1 reads only the first peakmap, and NREAD=0
      % will inhibits reading (default: NREAD=-1).
      %
      % If specified, BLOCK_FILE or PEAKMAP_FILE loads the SFDBBLOCKs
      % or PEAKMAPs respectively into the SFDB object that returns.
      % Both, just one, neither can be specified. To inhibit loading
      % pass ~ or ''.

      if ~exist('nreads','var')
        % Count will never match -1
        nreads = -1;
      end
      % Mutable object to be returned
      obj = Sfdb();

      % Possible TODOs:
      %   * Run Parallel

      % Short-circuits if variable exists, and inhibits if empty string.
      if (exist('block_file','var') || strcmp(block_file, ''))
        % Iterate through list
        for i = 1:length(block_file)
          obj.read_sfdb(block_file{i}, nreads);
          % Make sure certain number is read
          nreads = nreads - 1;
        end
      end

      % Same thing for peakmap
      if (exist('peakmap_file','var') || strcmp(peakmap_file, ''))
        % Iterate through list
        for i = 1:length(peakmap_file)
          obj.read_peakmap(peakmap_file{i}, nreads);
          % Make sure certain number is read
          nreads = nreads - 1;
        end
      end

    end

  end

  methods

    function obj = Sfdb()
      % SFDB constructor

      % Variable doesn't memoize by default
      obj.avg_count = 0;
    end

    function l = block_count(obj)
      % Number of blocks
      % A simple function for compatibility
      l = length(obj.block);
    end

    function l = peakmap_count(obj)
      % Number of peakmaps
      % A simple function for compatibility
      l = length(obj.peakmap);
    end

    function read_sfdb(obj, file, nreads)
      % Loads the first NREADS (if specified) or all the SFDBBLOCKs in
      % FILE into OBJ

      if ~exist('nreads','var')
        % BLOCK_COUNT will never match -1
        nreads = -1;
      end
      if exist('file','var')
        obj.block_file{end + 1} = file;
      end

      fid = fopen(obj.block_file{end});

      % Alias to simplify code readability
      n = obj.block_count;

      % Continue until eof or at specified iteration
      while (~feof(fid) && n ~= nreads)
        sfdbBlock = SfdbBlock.read(fid);

        % Check if valid block was read
        if (sfdbBlock.header.eof == 0)
          % Increase count
          % count = ++n
          n = n + 1;
          % Save excursion
          obj.block{n} = sfdbBlock;
        else
          % Stop loop if file ends
          nreads = n;
        end
      end

      fclose(fid);

    end

    function read_peakmap(obj, file, nreads)
      % Loads the first NREADS (if specified) or all the PEAKMAPs in
      % FILE into OBJ

      if ~exist('nreads','var')
        % PEAKMAP_COUNT will never match -1
        nreads = -1;
      end
      if exist('file','var')
        obj.peakmap_file{end + 1} = file;
      end

      fid = fopen(obj.peakmap_file{end});

      % Add header information
      obj.nfft = fread(fid, 1, 'int32');
      obj.sfr = fread(fid, 1, 'double');
      obj.lfft2 = fread(fid, 1, 'int32');
      obj.inifr = fread(fid, 1, 'double');

      obj.dfr = obj.sfr / (obj.lfft2 * 2);

      % Alias to simplify code readability
      n = obj.peakmap_count;

      % Continue until eof or at specified iteration
      while (~feof(fid) && n ~= nreads)
        % Increase count
        % count = ++n
        n = n + 1;
        % Saves excursion
        obj.peakmap{n} = Peakmap.read(fid, obj.dfr);
      end

      fclose(fid);

    end

    function inject(obj, f, amp, deltaf)
      % Injects a sinusoidal signal with
      %
      %  - F : frequency (default 200 Hz)
      %  - AMP : amplification factor (default 1)
      %  - DELTAF : A function of original frequency and time that
      %  outputs a new frequency (default: g(f, x) = f)

      if ~exist('amp', 'var')
        amp = 1;
      end
      if ~exist('f', 'var')
        f = 200;
      end
      if ~exist('deltaf', 'var')
        % Identity composed with first
        deltaf = @(f, t) f;
      end

      T = 0;
      for i = 1:obj.block_count
        % Domain of signal
        t = length(obj.block{i}.sft);
        % Sampling rate
        fs = .5/obj.block{i}.header.deltanu;
        injSignal = Signal.sin(f, fs, t, 1, T, deltaf);
        % Offset index start
        T = T + t;

        % Add signal to SFT
        obj.block{i}.inject(injSignal, amp);
      end

    end

    function simsour = inject_doppler(obj, min, max, N, amp)
      % Injects a Doppler shifted signal
      %
      % Within a frequency range [MIN, MAX], inject N signals with a
      % doppler shift.
      %
      % REQUIRES The SNAG Library

      freq_band = [min, max];

      % Only need to call on the first SFT
      [PARS, simsour] = Createmodulatedsignals(obj.block{1}.header ...
                                               , N, freq_band);

      % Optional amplitude
      if ~exist('amp', 'var')
        amp = 1;
      end

      % Save metadata for correction
      obj.simsour = simsour;

      % Iterate through all SFTs
      for i = 1:obj.block_count
        block = obj.block{i};
        head = block.header;
        dnu = head.deltanu;

        % AddsignalsinoneFFT_sub is a sub-sampled version of
        % AddsignalsinoneFFT which creates N SFTs to inject
        [simN] = AddsignalsinoneFFT(head, dnu, N, PARS, simsour, 2);

        % Inject signals into SFT
        block.sft = block.sft + (simN * amp);
      end
    end

    function [pm, f] = correct_doppler(obj, N, tol)
      % Correct peakmap for the Nth signal injected by INJECT_DOPPLER
      %
      % Return cell array of peaks PM localized around corrected
      % signal F with width TOL.
      %
      % REQUIRES The SNAG Library

      if ~exist('tol', 'var')
        tol = .5;
      end

      % Remember injection information
      simsour = obj.simsour;
      % Get original frequency (what we want to correct to)
      f = simsour(N).f0;

      % Get position (only needed once)
      alpha = simsour(N).a;
      delta = simsour(N).d;
      r = astro2rect([alpha,delta],0);

      % We don't know how many peaks there will be
      pm = [];

      for i = 1:obj.peakmap_count
        block = obj.block{i};
        head = block.header;
        dnu = head.deltanu;

        % Get velocity of detector
        vel = [head.vx_eq; head.vy_eq; head.vz_eq];
        % And compute the inner product and add 1 so it is a
        % coefficient. V1 should be a scalar
        v1 = (r * vel) + 1;

        % Divide each normalized squared frequency by V1
        pm{i} = obj.peakmap{i}.bb ./ v1;

        % Find indices nearby
        indices = find(pm{i} >= f-tol & pm{i} <= f+tol);

        % Return peaks near initial frequency
        pm{i} = pm{i}(indices);
      end
    end

    function amp = amplitude_here(obj, f, tol)
      % Amplitudes at a frequency
      %
      % At a frequency F with a half bandwidth of TOL, find the amplitudes.
      % If TOL is not provided it will be within the resolution of the SFT

      amp = [];
      for i = 1:obj.block_count
        deltanu = obj.block{i}.header.deltanu;
        if exist('tol', 'var')
          tol_tmp = tol;
        else
          tol_tmp = 0;
        end

        % Indices within bandwidth
        match_indices = floor((f - tol_tmp) / deltanu):ceil((f + tol_tmp) / deltanu);
        % Selects the height of amplitudes here
        [data, ~] = obj.block{i}.data;
        match_amp = data(match_indices);
        % Append amplitudes to higher scoped list
        amp = [amp, match_amp];
      end
      amp = mean(amp);
    end

    function count = count_peaks_here(obj, f, tol)
      % Counts peaks along a frequency
      %
      % Goes along all peakmaps and checks for peaks at a frequency F
      % with a tolerance of TOL.
      % If TOL is not provided it will be within the resolution of the SFT

      count = 0;
      for i = 1:obj.peakmap_count
        if exist('tol', 'var')
          tol_tmp = tol;
        else
          tol_tmp = obj.peakmap{i}.deltanu;
        end

        count = count + sum(abs(obj.peakmap{i}.bb - f) < tol_tmp);
      end

    end

    function pks = peaks_here(obj, f, tol)
      % Averages the amplitude of peaks
      %
      % Goes along all peakmaps and averages for peaks at a frequency F
      % with a tolerance of TOL.
      % If TOL is not provided it will be within the resolution of the SFT

      pks = [];
      for i = 1:obj.peakmap_count
        if exist('tol', 'var')
          tol_tmp = tol;
        else
          tol_tmp = obj.peakmap{i}.deltanu;
        end

        pm = obj.peakmap{i}.bb;

        % Find indices within a tolerance
        match_indices = find((pm.bb - f) < tol_tmp);
        % Selects the height of peaks here
        match_peaks = pm.(match_indices);
        % Append peaks to the data
        pks = [pks, match_peaks];
      end
    end

    function rate = efficiency_doppler(obj, f, amp, tau, N)
      % Calculates the efficiency of every injection of INJECT_DOPPLER
      %
      % Injects signals at frequency F with given optional parameters
      % to CREATE_PEAKMAP injected at a certain amplitude AMP.

      % Check defaults
      if ~exist('tau', 'var')
        tau = 0.02; %0.02 Hz
      end
      if ~exist('amp', 'var')
        amp = 10e-5;
      end
      if ~exist('N', 'var')
        N = length(f);
      end
      % Create a new class for repeated computations
      sfts = Sfdb();
      for j = 1:obj.block_count
        % Copy by value
        sfts.block{j} = SfdbBlock(obj.block{j}.header, 0, 0, ...
                                  obj.block{j}.sft);
      end

      % Pre-allocate
      rate = ones(1,N);
      simsour = [];

      for i = 1:N
        % Inject a signal at every frequency for lowish amplitude
        simsour{i} = sfts.inject_doppler(f(i), f(i), i, amp);
      end

      % Generate a peak map for this amplitude
      sfts.peakmap = sfts.create_peakmap(2.5, tau);

      for i = 1:N
        sfts.simsour = simsour{i};
        % Correct doppler
        [pm, ~] = sfts.correct_doppler(i, obj.block{1}.header.deltanu);
        % Count the amount of peaks at every frequency
        counts = length([pm{:}]);

        % How many peaks found at a certain frequency over the expected amount
        % Each peak count is also shifted by thave background level of peaks
        rate(i) = (counts - sfts.avg_peak_count) / (sfts.peakmap_count);
      end

      % Average efficiency for frequency
      rate = sum(rate) / N;
    end

    function rate = efficiency(obj, f, amp, tau, maxage, threshold)
      % Calculates the efficiency of every injection
      %
      % Injects signals at frequency F with given optional parameters
      % to CREATE_PEAKMAP injected at a certain amplitude AMP.

      % Check defaults
      if ~exist('f', 'var')
        f = 10:1024;
      end
      if ~exist('tau', 'var')
        tau = 0.02; %0.02 Hz
      end
      if ~exist('maxage', 'var')
        maxage = tau / obj.block{1}.header.deltanu; %#ok<*PROPLC>
      end
      if ~exist('threshold', 'var')
        threshold = 2.5;
      end
      if ~exist('amp', 'var')
        amp = 10e-5;
      end
      % Create a new class for repeated computations
      sfts = Sfdb();
      for j = 1:obj.block_count
        % Copy by value
        sfts.block{j} = SfdbBlock(obj.block{j}.header, 0, 0, ...
                                  obj.block{j}.sft);
      end

      % Inject a signal at every frequency for lowish amplitude
      arrayfun(@(freq) sfts.inject(freq, amp), f);
      % Generate a peak map for this amplitude
      sfts.peakmap = sfts.create_peakmap(threshold, tau, maxage);
      % Count the amount of peaks at every frequency
      counts = arrayfun(@(freq) sfts.count_peaks_here(freq), f);
      % How many peaks found at a certain frequency over the expected amount
      % Each peak count is also shifted by the background level of peaks
      rate = (counts - sfts.avg_peak_count) / sfts.peakmap_count;
    end

    function avg = avg_peak_count(obj)
      % Averages all peaks for every frequency bin

      if (obj.avg_count == 0)
        count = 0;
        total = 0;
        for i = 1:min(obj.block_count,obj.peakmap_count)
          [~, xdata] = obj.block{i}.data();
          total = total + length(xdata);
          count = count + length(obj.peakmap{i}.bb);
        end
        avg = count / total;

      else
        avg = obj.avg_count;
      end
    end

    function peakmaps = create_peakmap (obj, ...
                                        threshold, ...
                                        tau, ...
                                        maxage, ...
                                        f_begin, ...
                                        regen)
      % Generates peakmaps from SFDBBLOCKs with specified parameters
      %
      % REGEN will determine whether to regenerate power sepctrum
      % (default: true)

      obj.peakmap_count;

      if ~exist('threshold', 'var')
        threshold = 2.5;
      end
      if ~exist('f_begin', 'var')
        f_begin = 0;
      end
      if ~exist('tau', 'var')
        tau = 0.02; %0.02 Hz
      end
      if ~exist('regen', 'var')
        % Regenerate power spectrum by default
        regen = true;
      end

      % Preallocate array
      peakmaps = [];
      % Iterate through all blocks
      % block_count should equal length of block
      for i = 1:obj.block_count
        % Create a read only alias
        block = obj.block{i};

        if ~exist('maxage', 'var')
          maxage_tmp = tau / block.header.deltanu; %#ok<*PROPLC>
        else
          maxage_tmp = maxage;
        end

        if regen
          % Generate power spectrum from SFT data
          pow_spec = block.generate_pow_spec(tau, maxage_tmp, false);
        else % Use existing AR Spectrum
          pow_spec = block.ARspectrum;
        end

        peakmaps{end + 1} = Peakmap.create(block, ...
                                           threshold, ...
                                           f_begin, ...
                                           pow_spec); %#ok<*AGROW>

      end

    end

  end

end
